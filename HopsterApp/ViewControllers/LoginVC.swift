//
//  LoginVC.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/22/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Auto move textviews with keyboard
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(gesture:)))
        view.addGestureRecognizer(tapGesture)
        self.addObservers()
    }

    @IBAction func onClickSignInButton(_ sender: Any) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.loadHomeView()
        }
    }
    
    @IBAction func onClickCreateAccountButton(_ sender: Any) {
        self.performSegue(withIdentifier: createAccountSegue, sender: self)
    }
}

extension LoginVC {
    
    @objc func didTapView(gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { notification in
            self.keyboardWillShow(notification: notification)
        }
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { _ in
            self.keyboardWillHide()
        }
    }

    func keyboardWillShow(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
        }
        
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: frame.height + 20.0, right: 0)
        scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide() {
        scrollView.contentInset = UIEdgeInsets.zero
    }
}
