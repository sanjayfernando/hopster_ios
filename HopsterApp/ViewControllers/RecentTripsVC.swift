//
//  RecentTripsVC.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/23/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import UIKit

class RecentTripsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var recentTrips: [RecentTrip] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        self.tableView.register(UINib(nibName: "RecentTripCell", bundle: nil), forCellReuseIdentifier: "RecentTripCell")
        
        // Get test data
        recentTrips = TestData.getRecentTrips()
    }

    @IBAction func addNewTrip(_ sender: Any) {
        self.performSegue(withIdentifier: addTripSegue, sender: nil)
    }
}

extension RecentTripsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentTrips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentTripCell", for: indexPath) as! RecentTripCell
        cell.setData(data: self.recentTrips[indexPath.row])
        return cell
    }
}

extension RecentTripsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        self.performSegue(withIdentifier: viewTripSegue, sender: nil)
    }
}
