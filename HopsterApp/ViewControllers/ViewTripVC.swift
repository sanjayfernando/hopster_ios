//
//  ViewTripVC.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/24/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import UIKit

class ViewTripVC: UIViewController {

    @IBOutlet weak var from: UITextField!
    @IBOutlet weak var to: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onClickEditButton(_ sender: Any) {
        self.performSegue(withIdentifier: editTripSegue, sender: nil)
    }
    
}
