//
//  ForgotPasswordVC.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/23/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onClickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
