//
//  EditTripVC.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/25/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import UIKit

class EditTripVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onClickCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickSaveButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
