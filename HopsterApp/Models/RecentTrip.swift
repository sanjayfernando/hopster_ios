//
//  RecentTrip.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/23/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import Foundation

class RecentTrip {
    var from = ""
    var to = ""
    var date = ""
    var time = ""
    var format = ""
    var distance = "128 km"
    var noOfPassengers = 0
    
    
    init(from: String, to: String, date: String, time: String, format: String, distance: String, noOfPassengers: Int) {
        self.from = from
        self.to = to
        self.date = date
        self.time = time
        self.format = format
        self.distance = distance
        self.noOfPassengers = noOfPassengers
    }
}
