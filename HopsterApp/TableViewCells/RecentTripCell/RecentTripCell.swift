//
//  RecentTripCell.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/23/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import UIKit

class RecentTripCell: UITableViewCell {

    @IBOutlet weak var calenderIcon: UIImageView!
    @IBOutlet weak var passengerIcon: UIImageView!
    @IBOutlet weak var carIcon: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var formatLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var noOfPassengersLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        calenderIcon.tintColor = UIColor.darkGray
        passengerIcon.tintColor = UIColor.darkGray
        carIcon.tintColor = UIColor.darkGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data: RecentTrip) {
        self.time.text = data.time
        self.formatLabel.text = data.format
        self.fromLabel.text = data.from
        self.toLabel.text = data.to
        self.dayLabel.text = data.date
        self.distanceLabel.text = data.distance
        self.noOfPassengersLabel.text = "\(data.noOfPassengers)"
    }
    
}
