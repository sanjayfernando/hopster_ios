//
//  Constants.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/23/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import Foundation

// Segues
let createAccountSegue = "createAccountSegue"
let homeSegue          = "homeSegue"
let addTripSegue       = "addTripSegue"
let editTripSegue      = "editTripSegue"
let viewTripSegue      = "viewTripSegue"

// Tabbar Conrtollers
let homeTabbarController = "homeTabbarController"

// Storyboards
let mainStoryboard = "Main"
