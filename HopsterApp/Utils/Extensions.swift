//
//  Extensions.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/23/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func lightBlue() -> UIColor {
        return UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 218.0/255.0, alpha: 1.0)
    }
}
