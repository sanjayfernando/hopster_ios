//
//  TestData.swift
//  HopsterApp
//
//  Created by Chandimal, Sameera on 3/23/18.
//  Copyright © 2018 Hopster. All rights reserved.
//

import Foundation

class TestData {
    
    static func getRecentTrips() -> [RecentTrip] {
        var trips: [RecentTrip] = []
        
        let trip1 = RecentTrip(from: "Malabe, Kaduwela", to: "Orion City, Orugodawaththa", date: "Daily", time: "8.30", format: "AM", distance: "20 Km", noOfPassengers: 2)
        
        let trip2 = RecentTrip(from: "Orion City, Orugodawaththa", to: "Malabe, Kaduwela", date: "Daily", time: "6.30", format: "PM", distance: "20 Km", noOfPassengers: 1)
        
        let trip3 = RecentTrip(from: "Malabe, Kaduwela", to: "Rathnapura, Nivithigala", date: "Saturday", time: "8.30", format: "AM", distance: "100 Km", noOfPassengers: 1)
        
        //let trip4 = RecentTrip(from: "Rathnapura, Nivithigala", to: "Malabe, Kaduwela", date: "Sunday", time: "6.00", format: "PM", distance: "100 Km", noOfPassengers: 0)
        
        trips.append(contentsOf: [trip1, trip2, trip3])
        
        return trips
    }
}
